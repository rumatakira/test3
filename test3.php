<?php

/** @author Kirill A.Lapchinsky rumatakira74@gmail.com
 *  @copyright 2020 Kirill A.Lapchinsky All Rights Reserved
 */

$openSpolerExists = null;
$resultArray = [];

$file_lines = file('givenText.txt');

foreach ($file_lines as $line) {
    $spoilerAmountInLine = substr_count($line, '[SPOILER');
    $closeSpoilerAmountInLine = substr_count($line, '[/SPOILER]');
    // no [SPOILER], no [/SPOILER]
    if (!$spoilerAmountInLine and !$closeSpoilerAmountInLine) {
        $resultArray[] = $line;
        // first [SPOILER]
    } elseif ($spoilerAmountInLine == 1 and !$openSpolerExists) {
        $openSpolerExists = 1;
        $resultArray[] = $line;
        // next [SPOILER]
    } elseif ($spoilerAmountInLine == 1 and $openSpolerExists == 1) {
        $resultArray[] = '[/SPOILER]';
        $resultArray[] = "\r\n\r\n";
        $resultArray[] = $line;
        // [/SPOILER]
    } elseif ($closeSpoilerAmountInLine > 0 and $openSpolerExists == 1) {
        // multiplly [/SPOILER] in line
        if ($closeSpoilerAmountInLine > 1) {
            $resultArray[] = '[/SPOILER]';
        } else {
            $resultArray[] = $line;
        }
        $openSpolerExists = null;
    }
}
// write rezult to new file
file_put_contents('rezultText.txt', $resultArray, LOCK_EX);
